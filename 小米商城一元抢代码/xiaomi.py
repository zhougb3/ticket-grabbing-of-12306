from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

class GrabGoods(object):
    def __init__(self):
        self.login_url = 'https://account.xiaomi.com/pass/serviceLogin'
        self.shoppingList = 'https://static.mi.com/cart/'
        self.good_url = 'https://item.mi.com/product/8806.html'
        self.order_url = "https://order.mi.com/buy/checkout"
        self.driver = webdriver.Chrome()

    def login(self):
        self.driver.get(self.login_url)
        print('进行登录了')

    def order_goods(self):
        # 进入商品页面，点击立即抢购
        self.driver.get(self.good_url)
        WebDriverWait(self.driver, 1000).until(EC.presence_of_element_located((By.CLASS_NAME,'J_seckillBuyBtn')))
        qianggou = self.driver.find_element_by_class_name("J_seckillBuyBtn")
        qianggou.click()
        # 等待进入购物车，点击去结算
        WebDriverWait(self.driver, 1000).until(EC.url_to_be(self.shoppingList))
        WebDriverWait(self.driver, 1000).until(EC.presence_of_element_located((By.ID,'J_goCheckout')))
        ConBotton = self.driver.find_element_by_id('J_goCheckout')
        ConBotton.click()
        # 等待进入下单界面，点击去结算
        WebDriverWait(self.driver, 1000).until(EC.url_to_be(self.order_url))
        WebDriverWait(self.driver, 1000).until(EC.presence_of_element_located((By.ID,'J_addressList')))
        WebDriverWait(self.driver, 1000).until(EC.presence_of_element_located((By.ID,'J_checkoutToPay')))
        address = self.driver.find_element_by_xpath("//div[@id='J_addressList']/div[1]")
        address.click()
        order = self.driver.find_element_by_id("J_checkoutToPay")
        order.click()            
        return

if __name__ == '__main__':
    spider = GrabGoods()