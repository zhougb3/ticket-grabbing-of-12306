# Ticket Grabbing Of 12306

### 开发环境

操作系统：windows10

浏览器：Google浏览器（66.0版本）

Chromedriver： v2.40版本

编程语言：Python3.7.1

### 环境配置

```
pip install selenium
```

将下载的chromedriver.exe放进文件夹，再配置进path环境变量，Linux下把下载好的文件放在 /usr/bin 目录下就可以了。

Google浏览器和Chromedriver版本对应关系和下载地址参考如下：

[selenium 安装与 chromedriver安装](https://www.cnblogs.com/technologylife/p/5829944.html)

[2018 Selenium Chrome版本与chromedriver兼容版本对照表](https://blog.csdn.net/yoyocat915/article/details/80580066)

### 代码解析

##### spider.py

复制于[爬虫实战篇---12306抢票爬虫](https://www.cnblogs.com/518894-lu/p/9158843.html)

##### spiderUpdate.py

修改了类内部变量`initmy_url`，原先的url可能已经失效不起作用，并加上while循环当无票时循环刷新。

##### spiderFinally.py

1. 参数改为全局变量，每次都要命令行输入车次信息太烦了
2. 支持学生票和成人票一起抢购了（原先代码不支持学生票）
3. 原先代码由于缩进问题，当有多个乘车人时也只是买了一个人的票，改进了这个问题。

### 问题改进

1. 当多个乘车人而只有一张票时，下单失败可能代码会停止运行
2. 并没有考虑列车优先顺序的问题（不停刷新，怎么确定真正的优先顺序，改变循环的顺序是否有意义）
3. 购买学生票时是在普通票里面刷新购买的，并没有在学生票里面刷新（普通票也可以买学生票，而且普通票和学生票的数据好像不同步，很多情况会出现一方有一方没有的情况）要在学生票里面刷学生票可能要更改一下代码了
4. 下单成功后并没有通知，可以增加一个发邮件的功能，实现邮件通知付款。


### 参考

[selenium中文文档](https://selenium-python-zh.readthedocs.io/en/latest/locating-elements.html)

[无头浏览器入门](https://juejin.im/post/59e5a86c51882578bf185dba)

[Python系列教程](http://wiki.jikexueyuan.com/project/python-crawler-guide/the-configuration-of-scrapy.html)



